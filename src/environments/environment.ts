// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDsYWAosBfvUJMkphicWaqefH0IttxjGJ8",
  authDomain: "appsemprebella.firebaseapp.com",
  databaseURL: "https://appsemprebella.firebaseio.com",
  projectId: "appsemprebella",
  storageBucket: "appsemprebella.appspot.com",
  messagingSenderId: "327224492998",
  appId: "1:327224492998:web:1813fc60f676d535"
};
// Initialize Firebase
//firebase.initializeApp(firebaseConfig);