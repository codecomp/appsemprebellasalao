import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
export interface Login {
  id?: string;
  login: string;
  senha: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private loginsCollection: AngularFirestoreCollection<Login>;
 
  private logins: Observable<Login[]>;
 
  constructor(db: AngularFirestore) {
    this.loginsCollection = db.collection<Login>('logins');
 
    this.logins = this.loginsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getLogins() {
    return this.logins;
  }
 
  getLogin(id) {
    return this.loginsCollection.doc<Login>(id).valueChanges();
  }
 
  updateLogin(todo: Login, id: string) {
    return this.loginsCollection.doc(id).update(todo);
  }
 
  addLogin(todo: Login) {
    return this.loginsCollection.add(todo);
  }
 
  removeLogin(id) {
    return this.loginsCollection.doc(id).delete();
  }
}